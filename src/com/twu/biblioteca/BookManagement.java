package com.twu.biblioteca;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * FileName: BookManagement
 */

public class BookManagement {
    private ArrayList<Book> bookList = new ArrayList<Book>();
    public BookManagement() {
        initialList();
    }

    private void initialList() {
        Book book1 = new Book("book1","2001", "zhang san");
        Book book2 = new Book("book2","2002", "li si");
        Book book3 = new Book("book3","2001", "wang wu");
        Book book4 = new Book("book4","2002", "zhao liu");
        Book book5 = new Book("book5","2004", "zhang san");
        Book book6 = new Book("book6","2004", "zhang san");
        Book book7 = new Book("book7","2018", "zhang san");
        Book book8 = new Book("book8","2018", "zhang san");
        this.bookList.add(book1);
        this.bookList.add(book2);
        this.bookList.add(book3);
        this.bookList.add(book4);
        this.bookList.add(book5);
        this.bookList.add(book6);
        this.bookList.add(book7);
        this.bookList.add(book8);
    }

    public void showAllBooks() {
        System.out.printf("%25s, %15s, %20s%n", "name", "author", "publication year");
        for (int i = 0; i < bookList.size(); i++) {
            if (bookList.get(i).getAvailable()) {
                System.out.printf("%25s, %15s, %20s%n", bookList.get(i).getName(), bookList.get(i).getAuthor(), bookList.get(i).getPublicationYear());
            }
        }
    }

    public void checkoutBook() {
        String wantName = wantedBookName();
        for(int i=0;i<bookList.size();i++){
            if (bookList.get(i).getName().equalsIgnoreCase(wantName)
                    && bookList.get(i).getAvailable()) {
                bookList.get(i).setAvailable(false);
                System.out.println("Thank you!Enjoy your book.");
                return;
            }
        }
        System.out.println("Sorry, that book is not available");
    }

    public void returnBook() {
        String bookName = wantedBookName();
        for(int i=0;i<bookList.size();i++){
            if (bookList.get(i).getName().equalsIgnoreCase(bookName)) {
                bookList.get(i).setAvailable(true);
                System.out.println("Thank you for returning the book.");
                return;
            }
        }
        System.out.println("That is not a valid book to return");
    }

    private String wantedBookName() {
        Scanner input = new Scanner(System.in);
        System.out.println("please enter book's name：");
        return input.next();
    }
}