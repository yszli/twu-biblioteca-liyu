package com.twu.biblioteca;

/**
 * FileName: Book
 */

public class Book {
    private String name;
    private String publicationYear;
    private String author;
    private Boolean available;

    public Book(String bookName, String publicationYear, String author) {
        this.name = bookName;
        this.publicationYear = publicationYear;
        this.author = author;
        this.available = true;
    }

    public String getName() {
        return name;
    }

    public String getPublicationYear() {
        return publicationYear;
    }

    public String getAuthor() {
        return author;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean state) {
        this.available = state;
    }
}