package com.twu.biblioteca;
import java.util.Scanner;

public class BibliotecaApp {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Printer printer = new Printer();
        BookManagement bookManagement = new BookManagement();
        printer.printWelcomeMessage();
        printer.printMenu();
        while (true) {
            int choice = scan.nextInt();
            if (choice == 4) {
                System.out.println("Thank you for using!See you!");
                return;
            }
            switch (choice)
            {
                case 1:
                    bookManagement.showAllBooks();
                    printer.printMenu();
                    break;
                case 2:
                    bookManagement.checkoutBook();
                    printer.printMenu();
                    break;
                case 3:
                    bookManagement.returnBook();
                    printer.printMenu();
                    break;
                default:
                    System.out.println("Please enter an valid option");
                    printer.printMenu();
                    continue;
            }
        }
    }
}


