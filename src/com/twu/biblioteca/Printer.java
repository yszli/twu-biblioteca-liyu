package com.twu.biblioteca;

/**
 * FileName: Printer
 */

public class Printer {
    public void printWelcomeMessage() {
        System.out.println("Welcome to Biblioteca. Your one-stop-shop for great book titles in Bangalore\n");
    }

    public void printMenu() {
        System.out.println("1. View a list of books");
        System.out.println("2. Checkout a book");
        System.out.println("3. Return a book");
        System.out.println("4. Quit the application");
        System.out.println("please make your choice: ");
    }
}